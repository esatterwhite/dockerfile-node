FROM ubuntu:10.04

WORKDIR /opt/

RUN apt-get update
RUN apt-get install -y build-essential libssl-dev uuid-dev wget curl git-core vim htop openjdk-6-jdk openjdk-6-jre openjdk-6-jre-lib openssh-client openssh-server libjpeg62 libjpeg62-dev libuuid1 libtiff4 libtiff4-dev libtool libgraphviz4 libgraphviz-dev libevent-dev libevent-core-1.4-2 libevent-extra-1.4-2 libevent-1.4-2 libxml2 libxml2-dev libxslt1.1 libxslt1-dev

ADD ./node-v0.10.30.tar.gz /opt/
ADD ./zeromq-3.2.4.tar.gz /opt/

# simple node binary install
WORKDIR /opt/node-v0.10.30-linux-x64/

RUN ln -s $PWD/lib/node_modules/npm/cli.js /usr/local/bin/npm
RUN ln -s $PWD/bin/node /usr/local/bin/
RUN echo node $(node -v) && echo npm $(npm -v)
#RUN npm install -g grunt jslint pm2

## zmq install
WORKDIR /opt/zeromq-3.2.4
RUN ./configure && make && make install && ldconfig
RUN bash -c 'touch ~/.bashrc && curl https://raw.githubusercontent.com/creationix/nvm/v0.17.1/install.sh | bash && source ~/.nvm/nvm.sh && nvm install 0.8 && nvm install 0.10 && nvm install 0.11 && nvm alias default 0.10 && nvm use default && npm install -g jshint grunt-cli bower'

WORKDIR /opt
